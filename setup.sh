#!/usr/bin/env bash
# Based on a gitpod-provided example

# Gitpod runs in a folder that’s not the script location
SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")

cd "$SCRIPT_PATH" || exit 3
cp -r ./.* "$HOME"

git config --global --add "include.path" "${HOME}/.gitconfig-common"
git config --global core.editor "nvim"

# Weird bug with user creds, work around it
sudo sed 's/%40/@/' -i /etc/apt/auth.conf.d/*
sh -c 'sudo apt update && sudo apt install -y neovim' & disown
