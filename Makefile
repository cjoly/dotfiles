all:
	cp ~/.config/git/config-common .gitconfig-common
	cp ~/.config/git/ignore .config/git/ignore
	cp ~/.config/nvim/ftplugin/gitrebase.vim .vim/ftplugin/
